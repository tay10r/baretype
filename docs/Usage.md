Using the API
=============

You first setup the frame on which you'd like to draw with.

```c
unsigned char frame_buf[4096];
unsigned int width = 128;
unsigned int height = 32;
/* Pitch is the number of bytes per line.
 * In this example, it's the same as the width. */
unsigned int pitch = width;

btFrame frame = {
  frame_buf,
  width,
  height,
  pitch
};
```

Then once that's done, you can initialize a graphics context structure.
The graphics context structure contains information like stroke width, pen position, and so on.
You'll need it for every rendering function in the library.

```c
btGraphicsContext gc = baretype.init(&frame);
```

Notice the `baretype` variable? It's a global structure containing pointers to the library functions.
The library functions are declared this way to minimize the number of symbols exported to the object file.

In order to draw a character, use the `drawChar` function.

```c
baretype.drawChar(&gc, 'g');
```

Since drawing a character involves several different shapes,
the drawing operations for these shapes have been exposed so
that more use can be obtained from the library.

Triangles can be drawn like this;

```
btPoint triangle[3] = {
  {    0,   0 },
  { 1024, 512 },
  {  128, 256 }
};

baretype.fillTriangle(&gc, triangle);
```

The points used in the drawing functions are declared in terms of font units.
It's important to distingush font units from pixel coordinates.

By default, a font unit is 1024th of an EM unit.

There is a specified number of pixels per EM.
By default, this is 24.
To change this value, use `baretype.setPixelsPerEM`.

Here's an example that sets the EM space to be a square of 480 pixels.

```c
baretype.setPixelsPerEM(&gc, 480);
```
