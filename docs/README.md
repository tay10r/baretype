BareType Documentation
======================

Welcome to the BareType documentation!

If you haven't already compiled the library with examples, [learn how](docs/Building.md).

Once that's done, you can [learn how to use](docs/Usage.md) it.

If you find an area lacking in documentation, please [start an issue](https://gitlab.com/tay10r/baretype/issues/new) about it.
