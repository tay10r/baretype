Typeface
========

The typeface format is a binary format that describes to the library how to draw a set of characters (or glyphs.)

The format is made up of tables. Some tables describe shapes like quadrilaterals, some describe bezier curves, and then glyphs.
Each table has a header at the beginning of the file that describes where it lies at within the file.

Note that all integers described in the format are encoded as little-endian.

### File Header Layout

| Field          | Type | Description                                                    |
|----------------|------|----------------------------------------------------------------|
| Magic Number   | U32  | A magic number to verify that this is a valid typeface.        |
| Charcode Begin | U32  | The beginning of the supported character code range.           |
| Charcode Count | U32  | The number of supported character codes.                       |
| Baseline       | U16  | The number of units from the top that the baseline resides at. |

A valid magic number is 0x4f18a4cd.

### Table Header Layout

A table header is a data structure in the following format.

| Field        | Type | Description                                  |
|--------------|------|----------------------------------------------|
| Table Offset | U32  | The offset of the table within this file.    |
| Entry Count  | U32  | The number of entries in this table.         |
| Entry Size   | U32  | The size of a single entry within the table. |

### Table Slice Layout

| Field  | Type | Description                                                                     |
|--------|------|---------------------------------------------------------------------------------|
| Index  | U32  | The index of the starting entry in the implicit table.                          |
| Count  | U32  | The number of entries in the table, after the offset, to include in this slice. |

### Point Layout

| Field | Type | Description               |
|-------|------|---------------------------|
| X     | I16  | The X value of the point. |
| Y     | I16  | The Y value of the point. |

### Table Header Order

The table headers, starting at the beginning of the file, are in this order:

 - Glyphs
 - Triangles
 - Quadrilaterals
 - Lines
 - Quadratic Bezier Curves
 - Cubic Bezier Curves

The total size occupied by the table headers at the beginning of the file is 64 bytes.

### Glyph Format

A glyph consists of several table slices, in the following order:

 - Triangles
 - Quadrilaterals
 - Lines
 - Quadratic Bezier Curves
 - Cublic Bezier Curves

After the 40 bytes of table slices:

| Field   | Type | Description                                                                   |
|---------|------|-------------------------------------------------------------------------------|
| Advance | I16  | Indicates how much the pen should move horizontally after drawing this glyph. |

### Triangle Format

An entry in the triangle consists of only three points.
The points should be in clockwise winding order, otherwise they wont render properly.

The size of this structure should be 12 bytes.

### Quadrilateral Format

The format for specifying a quadrilateral, like the triangle, consists of only points.
A quadrilateral is described by four points. This points must be in clockwise winding order.

The size of this structure should be 16 bytes.

### Line Format

An entry in the line table consists of two points followed by a 16-bit stroke width.

The size of this structure should be 10 bytes.

### Quadratic Bezier Curve

A quadratic Bezier curve definition consists of three points followed by a 16-bit stroke width.

The size of this structure should be 14 bytes.

### Cubic Bezier Curve

A cubic Bezier curve definition consists of four points followed by a 16-bit stroke width.

The size of this structure should be 18 bytes.
