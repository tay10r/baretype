Building the Library
====================

To build the project, you can use:

  - manual compilation
  - GNU Make
  - CMake

### Manual Compilation

Since the library is only a single C file, compiling manual is just a manner
of typing in the command once.

On macOS and Linux, you can use GCC.

```
gcc baretype.c -c
```

On Windows, similiarly using MSVC:

```
cl.exe baretype.c
```

You can use Clang on most platforms too:

```
clang baretype.c -c
```

Then just link the object file.

### Building with GNU Make

You can build the library using the default Makefile.

```
make
```

This will generate a static library, called `libbaretype.a`.

Link it with the program you want to use.

To build the example programs, build the `examples` target.

```
make examples
```

Optionally you can also install the library and header file.

```
make install
```

By default, it installs into `~/.local`.

You can change the install location using the `PREFIX` variable.

```
sudo make install PREFIX=/usr/local
```

If you're cross compiling, just set the `CROSS_COMPILE` variable
to the prefix of the compiler you're using.

For example:

```
make CROSS_COMPILE=aarch64-linux-gnu-
```

Will compile for 64-bit ARM using `aarch64-linux-gnu-gcc`.

To build with debugging info, pass `-g` to `CFLAGS`.

```
make CFLAGS=-g
```

To build with optimizations:

```
make CFLAGS=-O3
```

### Building with CMake

To build it with CMake, create a build directory and enter it with a terminal.

```
mkdir build
cd build
```

Then run CMake.

```
cmake ..
```

If you'd like to build the examples too, then do it like this:

```
cmake .. -DBARETYPE_EXAMPLES=ON
```

If you have Qt5 installed and you'd like to build the typeface designer, use the `-DBARETYPE_DESIGNER=ON` option.

```
cmake .. -DBARETYPE_DESIGNER=ON
```

Then build the project like this:

```
cmake --build .
```

To install CMake on Debian and Ubuntu:

```
sudo apt install cmake
```

On Arch Linux:

```
sudo pacman -S cmake
```

On Windows, you'll have to download it from their website: https://cmake.org/download
