#include "example_crt.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** Indicates whether or not an argument
 * is a specified option.
 * @param arg The argument to check.
 * @param s The short version of the option.
 * @param l The long version of the option.
 * @returns Non-zero on a match, zero otherwise.
 * */
static int IsOpt(const char* arg, char s, const char* l) {

  if (arg[0] != '-') {
    return 0;
  }

  if ((arg[1] == s) && (arg[2] == 0)) {
    return 1;
  }

  if (arg[1] != '-') {
    return 0;
  }

  return strcmp(arg + 2, l) == 0;
}

/** Exports a frame as an image file.
 * @param frame_buf The frame buffer containing the pixel data.
 * @param width The width of the frame.
 * @param height The height of the frame.
 * @param pitch The number of bytes per line.
 * @param filename The name of the file to create.
 * @returns Zero on success, non-zero on failure.
 * */
static int ExportToFile(const unsigned char* frame_buf,
                        unsigned long int width,
                        unsigned long int height,
                        unsigned long int pitch,
                        const char* filename) {

  FILE* ppmfile = fopen(filename, "wb");
  if (!ppmfile) {
    fprintf(stderr, "Failed to open '%s' for writing (%s)\n", filename, strerror(errno));
    return EXIT_FAILURE;
  }

  fprintf(ppmfile, "P6\n");
  fprintf(ppmfile, "%lu\n", width);
  fprintf(ppmfile, "%lu\n", height);
  fprintf(ppmfile, "255\n");

  unsigned char pixel[3];

  for (unsigned long int y = 0; y < height; y++) {

    for (unsigned long int x = 0; x < width; x++) {

      unsigned char value = frame_buf[(pitch * y) + x];
      pixel[0] = value;
      pixel[1] = value;
      pixel[2] = value;

      if (fwrite(pixel, 3, 1, ppmfile) != 1) {
        fclose(ppmfile);
        return EXIT_FAILURE;
      }
    }
  }

  fclose(ppmfile);

  return EXIT_SUCCESS;
}

/** Creates an image file name based on the program name.
 * @param prog The program name to make an image name from.
 * @returns A dynamically allocated image filename.
 * */
static char* MakeFilename(const char* prog) {

  size_t len = strlen(prog) + sizeof(".ppm") - 1;

  char* out = malloc(len + 1);
  if (!out) {
    return out;
  }

  sprintf(out, "%s.ppm", prog);

  return out;
}

int main(int argc, char** argv) {

  const char* prog = argv[0];

  const char* width_arg = "640";

  const char* height_arg = "480";

  for (int i = 1; i < argc; i++) {
    if (IsOpt(argv[i], 'w', "width")) {
      width_arg = argv[++i];
    } else if (IsOpt(argv[i], 'h', "height")) {
      height_arg = argv[++i];
    } else if (argv[i][0] == '-') {
      fprintf(stderr, "%s: Unknown option '%s'\n", prog, argv[i]);
      return EXIT_FAILURE;
    } else {
      fprintf(stderr, "%s: Trailing argument '%s'\n", prog, argv[i]);
      return EXIT_FAILURE;
    }
  }

  if (!width_arg) {
    fprintf(stderr, "%s: No value specified for width option.\n", prog);
    return EXIT_FAILURE;
  }

  if (!height_arg) {
    fprintf(stderr, "%s: No value specified for height option.\n", prog);
    return EXIT_FAILURE;
  }

  unsigned long int width = 0;

  if (sscanf(width_arg, "%lu", &width) != 1) {
    fprintf(stderr, "%s: Not a valid width value: %s\n", prog, width_arg);
    return EXIT_FAILURE;
  }

  unsigned long int height = 0;

  if (sscanf(height_arg, "%lu", &height) != 1) {
    fprintf(stderr, "%s: Not a valid height value: %s\n", prog, height_arg);
    return EXIT_FAILURE;
  }

  unsigned long int pitch = width;

  unsigned char* buf = calloc(1, pitch * height);
  if (!buf) {
    fprintf(stderr, "%s: Failed to allocate memory (%s)\n", prog, strerror(errno));
    return EXIT_FAILURE;
  }

  char* filename = MakeFilename(prog);

  ExampleMain(buf, width, height, pitch);

  int err = ExportToFile(buf, width, height, pitch, filename);
  if (err != 0) {
    fprintf(stderr, "%s: Failed to create '%s' (%s)\n", prog, filename, strerror(errno));
  }

  free(filename);

  free(buf);

  return err ? EXIT_FAILURE : EXIT_SUCCESS;
}
