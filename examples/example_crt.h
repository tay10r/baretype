#ifndef BARETYPE_EXAMPLES_EXAMPLE_CRT_H
#define BARETYPE_EXAMPLES_EXAMPLE_CRT_H

#ifdef __cplusplus
extern "C" {
#endif

/** This is the entry point to the example.
 * @param buf The frame buffer to draw on.
 * @param width The width of the frame.
 * @param height The height of the frame.
 * @param pitch The number of bytes per line.
 * */
void ExampleMain(unsigned char* buf,
                 unsigned long int width,
                 unsigned long int height,
                 unsigned long int pitch);

#ifdef __cplusplus
} /* extern "C" { */
#endif

#endif /* BARETYPE_EXAMPLES_EXAMPLE_CRT_H */
