Examples
========

This directory contains a set of examples.

In order to reduce the bloat code, most of the setup is done in `example_crt.c`.

The code that demonstrates the library usage will be in all other `.c` files,
starting in the function called `ExampleMain`.
