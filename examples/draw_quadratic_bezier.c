#include "example_crt.h"

#include <baretype.h>

void ExampleMain(unsigned char* buf,
                 unsigned long int width,
                 unsigned long int height,
                 unsigned long int pitch) {

  btFrame frame = {
    buf,
    width,
    height,
    pitch
  };

  btGraphicsContext gc = baretype.init(&frame);

  baretype.setPixelsPerEM(&gc, (height < width) ? height : width);

  int units_per_em = baretype.getUnitsPerEM(&gc);

  const btPoint points[] = {
    { units_per_em * 1 / 10, units_per_em * 1 / 10},
    { units_per_em * 7 / 10, units_per_em * 5 / 10 },
    { units_per_em * 2 / 10, units_per_em * 3 / 10 },
  };

  baretype.drawQuadraticBezier(&gc, points);
}
