#include "example_crt.h"

#include <baretype.h>

#include <stdio.h>

void ExampleMain(unsigned char* buf,
                 unsigned long int width,
                 unsigned long int height,
                 unsigned long int pitch) {

  const char text_str[] = "The quick brown fox jumps over the lazy dog.";

  btFrame frame = {
    buf,
    width,
    height,
    pitch
  };

  btGraphicsContext gc = baretype.init(&frame);

  for (unsigned int i = 0; i < (sizeof(text_str) - 1); i++) {
    btError err = baretype.drawChar(&gc, text_str[i]);
    if (err) {
      fprintf(stderr, "Failed to print '%c': %s\n", text_str[i], baretype.strerror(err));
      break;
    }
  }
}
