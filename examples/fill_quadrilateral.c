#include "example_crt.h"

#include <baretype.h>

void ExampleMain(unsigned char* buf,
                 unsigned long int width,
                 unsigned long int height,
                 unsigned long int pitch) {

  btFrame frame = {
    buf,
    width,
    height,
    pitch
  };

  btGraphicsContext gc = baretype.init(&frame);

  /* Let's make sure the triangle is very
   * visible by using most of the frame as
   * an EM unit. */

  /* Use the min */
  int pixels_per_em = (height < width) ? height : width;

  baretype.setPixelsPerEM(&gc, pixels_per_em);

  int units_per_em = baretype.getUnitsPerEM(&gc);

  /* These are the points that
   * make up the triangle. Make
   * sure that they're sorted CW. */
  const btPoint points[] = {
    { units_per_em * 1 / 5, units_per_em * 1 / 6 },
    { units_per_em * 3 / 4, units_per_em * 1 / 4 },
    { units_per_em * 5 / 7, units_per_em * 3 / 4 },
    { units_per_em * 1 / 4, units_per_em * 5 / 7 }
  };

  baretype.fillQuadrilateral(&gc, points);
}
