#include "example_crt.h"

#include <baretype.h>

void ExampleMain(unsigned char* buf,
                 unsigned long int width,
                 unsigned long int height,
                 unsigned long int pitch) {

  btFrame frame = {
    buf,
    width,
    height,
    pitch
  };

  btGraphicsContext gc = baretype.init(&frame);

  /* Let's make sure the triangle is very
   * visible by using most of the frame as
   * an EM unit. */

  /* Use the min */
  int pixels_per_em = (height < width) ? height : width;

  baretype.setPixelsPerEM(&gc, pixels_per_em);

  /* Declare the triangle.
   * Since we describe the triangle
   * using font units, we need to know
   * the number of units per EM so that
   * we can get the numbers right. */

  int units_per_em = baretype.getUnitsPerEM(&gc);

  /* These are the points that
   * make up the triangle. */
  const btPoint points[] = {
    { 0,                               0 },
    { units_per_em,     units_per_em / 3 },
    { units_per_em / 8, units_per_em / 2 }
  };

  baretype.fillTriangle(&gc, points);
}
