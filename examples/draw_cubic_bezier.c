#include "example_crt.h"

#include <baretype.h>

void ExampleMain(unsigned char* buf,
                 unsigned long int width,
                 unsigned long int height,
                 unsigned long int pitch) {

  btFrame frame = {
    buf,
    width,
    height,
    pitch
  };

  btGraphicsContext gc = baretype.init(&frame);

  baretype.setPixelsPerEM(&gc, (height < width) ? height : width);

  int units_per_em = baretype.getUnitsPerEM(&gc);

  const btPoint points[] = {
    { 0, 0 },
    { units_per_em / 2,  units_per_em / 16 },
    { units_per_em / 4,  units_per_em /  3  },
    { units_per_em / 16, units_per_em /  2  },
  };

  baretype.drawCubicBezier(&gc, points);
}
