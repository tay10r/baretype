/** @file baretype.c
 * @brief The implementation file for the BareType library.
 * See @ref baretype_impl for documentation on this file.
 * */

/** @defgroup baretype_impl Implementation Details
 * @brief Declarations related to the
 * internal implementation details of the
 * project. You only need to read about
 * code under this group if you plan on
 * modifying the typeface or the library.
 * */

#include "baretype.h"

/** The default number of pixels
 * in one EM unit.
 * @ingroup baretype_impl
 * */
#define DEFAULT_PIXELS_PER_EM 24

/** The default number of units
 * per one EM unit.
 * @ingroup baretype_impl
 * */
#define DEFAULT_UNITS_PER_EM 1024

/** The default stroke width.
 * @ingroup baretype_impl
 * */
#define DEFAULT_STROKE_WIDTH 32

/** The default number
 * of subpixels to render.
 * @ingroup baretype_impl
 * */
#define DEFAULT_SUBPIXELS 256

/** The maximum value for a dot.
 * @ingroup baretype_impl
 * */
#define DOT_MAX 255

/** Gets the minimum of two values.
 * @ingroup baretype_impl
 * */
#define MIN2(a, b) ((a < b) ? a : b)

/** Gets the maximum of two values.
 * @ingroup baretype_impl
 * */
#define MAX2(a, b) ((a > b) ? a : b)

/** Gets the minimum of three values.
 * @ingroup baretype_impl
 * */
#define MIN3(a, b, c) (MIN2(MIN2(a, b), c))

/** Gets the maximum of three values.
 * @ingroup baretype_impl
 * */
#define MAX3(a, b, c) (MAX2(MAX2(a, b), c))

/** References a section from
 * a table, such as the triangle
 * or cubic Bezier curve table.
 * @ingroup baretype_impl
 * */
struct btTableSlice {
  /** The starting location of
   * the table slice. */
  unsigned long int start;
  /** The number of entries part
   * of this table slice. */
  unsigned long int count;
};

/** A type definition for a table slice.
 * @ingroup baretype_impl
 * */
typedef struct btTableSlice btTableSlice;

/** Contains information on how to
 * draw a certain glyph.
 * @ingroup baretype_impl
 * */
struct btGlyph {
  /** A reference to this glyph's triangles. */
  btTableSlice triangles;
  /** A reference to this glyph's quadrilaterals. */
  btTableSlice quadrilaterals;
  /** A reference to this glyph's lines. */
  btTableSlice lines;
  /** A reference to this glyph's quadratic Bezier curves. */
  btTableSlice quadratic_bezier_curves;
  /** A reference to this glyph's cubic Bezier curves. */
  btTableSlice cubic_bezier_curves;
  /** The number of font units to move
   * to the right after drawing this glyph. */
  btInt advance;
};

/** A type definition for a glyph structure.
 * @ingroup baretype_impl
 * */
typedef struct btGlyph btGlyph;

extern const btTypeface baretype_typeface;

static btInt lengthOfVector(const btPoint* point);

static btInt distFromPoint(const btPoint* a, const btPoint* b);

static btTableSlice readTableSlice(const unsigned char* buf);

static unsigned short int readU16(const unsigned char* buf);

static unsigned long int readU32(const unsigned char* buf);

static btPoint scaleVectorTo(const btPoint* in, btInt length);

/** Calculates the perimeter of a line set.
 * This is used to approximate the length of quadratic
 * and cubic Bezier curves. It is then used to determine
 * how many lines the curve should be broken up into.
 * @param points The points making up the line set.
 * @param count The number of points in the point array.
 * @returns The length of the line set.
 * @ingroup baretype_impl
 * */
static btInt calcPerimeter(const btPoint* points, btSize count) {

  if (count <= 1) {
    return 0;
  } else if (count == 2) {
    return distFromPoint(points + 0, points + 1);
  }

  btInt length = 0;

  for (btSize i = 0; i < (count - 1); i++) {
    length += distFromPoint(points + i, points + i + 1);
  }

  length += distFromPoint(points + (count - 1), points + 0);

  return length;
}

/** Calculates the square root of an integer value.
 * @param n The value to get the square root of.
 * @returns The square root of @p n.
 * @ingroup baretype_impl
 * */
static btInt calcSqrt(btInt n) {

  n = (n < 0) ? -n : n;

  /* First, try to handle base cases. */

  switch (n) {
    case 0:
      return 0;
    case 1:
    case 2:
    case 3:
      return 1;
    case 4:
      return 2;
    default:
      break;
  }

  btInt start = 1;
  btInt end = n;
  btInt ans = 0;

  while (start <= end) {

    btInt mid = (start + end) / 2;

    btInt result = mid * mid;

    if (result == n) {
      return mid;
    } else if (result < n) {
      start = mid + 1;
      ans = mid;
    } else {
      end = mid - 1;
    }
  }

  return ans;
}

/** Calculates a point along a quadratic Bezier curve.
 * @param points The points making up the curve.
 * There must be at least three points in this point array.
 * @param t The distance along the curve to plot.
 * @param t_max The number of distance units in the curve.
 * @returns The point of the curve at the offset @p t.
 * @ingroup baretype_impl
 * */
static btPoint quadraticBezier(const btPoint* points, btInt t, btInt t_max) {

  btInt t_diff = t_max - t;

  btInt numerators[3] = {
    t_diff * t_diff,
    2 * t_diff * t,
    t * t
  };

  btInt t_max_2 = t_max * t_max;

  btPoint out = { 0, 0 };

  for (btSize i = 0; i < 3; i++) {
    out.x += (numerators[i] * points[i].x) / t_max_2;
    out.y += (numerators[i] * points[i].y) / t_max_2;
  }

  return out;
}

/** Calculates a cubic Bezier point.
 * @param points The four control points.
 * @param t The value of t.
 * @param t_max The maximum value of t.
 * @returns The point of the curve at the
 * specified value of @p t.
 * @ingroup baretype_impl
 * */
static btPoint cubicBezier(const btPoint* points, btInt t, btInt t_max) {

  btInt t_diff = t_max - t;

  btInt t_diff_2 = t_diff * t_diff;

  btInt t_diff_3 = t_diff_2 * t_diff;

  btInt t_2 = t * t;

  btInt numerators[4] = {
    t_diff_3,
    3 * t_diff_2 * t,
    3 * t_diff * t_2,
    t_2 * t
  };

  btInt t_max_3 = t_max * t_max * t_max;

  btPoint out = { 0, 0 };

  for (btSize i = 0; i < 4; i++) {
    out.x += (numerators[i] * points[i].x) / t_max_3;
    out.y += (numerators[i] * points[i].y) / t_max_3;
  }

  return out;
}

/** Calculates the offset points
 * for a single leg.
 * @param start The start of the leg.
 * The offset points will be translated
 * from this point.
 * @param end The end of the leg.
 * This is used to compute the normal.
 * @param stroke The distance between
 * the offset points.
 * */
static void legOffsets(const btPoint* start,
                       const btPoint* end,
                       btPoint* l_offset,
                       btPoint* r_offset,
                       btInt stroke) {

  btInt delta_x = end->x - start->x;
  btInt delta_y = end->y - start->y;

  btInt stroke_dx = ((stroke / 2) * delta_y) / calcSqrt((delta_x * delta_x) + (delta_y * delta_y));
  btInt stroke_dy = (-delta_x * stroke_dx) / delta_y;

  l_offset->x = start->x + stroke_dx;
  l_offset->y = start->y + stroke_dy;

  r_offset->x = start->x - stroke_dx;
  r_offset->y = start->y - stroke_dy;
}

/** Calculates the offsets for a line.
 * @param l_offsets The points for the "left" offset.
 * @param r_offsets The points for the "right" offset.
 * @param stroke The stroke width.
 * */
static void lineOffsets(const btPoint* points,
                        btPoint* l_offsets,
                        btPoint* r_offsets,
                        btInt stroke) {

  legOffsets(points + 0, points + 1, l_offsets + 0, r_offsets + 0, stroke);
  legOffsets(points + 1, points + 0, r_offsets + 1, l_offsets + 1, stroke);
}

/** Calculates points for the offsets
 * of a quadratic Bezier curve.
 * @param points The points making up the quadratic curve.
 * @param l_offsets The point array of "left" offsets to assign.
 * @param r_offsets The point array of "right" offsets to assign.
 * @param stroke The stroke width.
 * @ingroup baretype_impl
 * */
static void quadraticOffsets(const btPoint* points,
                             btPoint* l_offsets,
                             btPoint* r_offsets,
                             btInt stroke) {

  legOffsets(points + 0, points + 1, l_offsets + 0, r_offsets + 0, stroke);
  legOffsets(points + 2, points + 1, r_offsets + 2, l_offsets + 2, stroke);

  btInt a_delta_x = l_offsets[0].x - points[0].x;
  btInt a_delta_y = l_offsets[0].y - points[0].y;

  btInt b_delta_x = l_offsets[2].x - points[2].x;
  btInt b_delta_y = l_offsets[2].y - points[2].y;

  btPoint avg_delta = {
    (b_delta_x - a_delta_x) / 2,
    (b_delta_y - a_delta_y) / 2
  };

  btPoint scaled_avg_delta = scaleVectorTo(&avg_delta, stroke / 2);

  l_offsets[1].x = points[1].x - scaled_avg_delta.x;
  l_offsets[1].y = points[1].y - scaled_avg_delta.y;

  r_offsets[1].x = points[1].x + scaled_avg_delta.x;
  r_offsets[1].y = points[1].y + scaled_avg_delta.y;
}

/** Calculates the points for the "offset" curves.
 * @param points The original curve points.
 * There should be at least four points in this array.
 * @param l_offsets The point array for the "left" offsets.
 * @param r_offsets The point array for the "right" offsets.
 * @param width The width from the left to right offset curves.
 * This can also be thought of as the "stroke width".
 * */
static void cubicOffsets(const btPoint* points,
                         btPoint* l_offsets,
                         btPoint* r_offsets,
                         btInt width) {

  (void)width;

  for (btSize i = 0; i < 4; i++) {
    l_offsets[i] = points[i];
    r_offsets[i] = points[i];
  }
}

/** Creates a sub frame in which the next glyph can be drawn to.
 * @param gc An initialized graphics context instance.
 * @returns A frame in which the next glyph can be drawn to.
 * @ingroup baretype_impl
 * */
static btFrame createCellFrame(btGraphicsContext* gc) {

  btPoint start = gc->pen_position;

  btInt width = gc->pixels_per_em;

  if ((start.x + width) > ((btInt) gc->frame->width)) {
    width = gc->frame->width - start.x;
  }

  btInt height = gc->pixels_per_em;

  if ((start.y + height) > ((btInt) gc->frame->height)) {
    height = gc->frame->height - start.y;
  }

  btFrame frame = {
    gc->frame->buf + (gc->frame->pitch * start.y) + start.x,
    width,
    height,
    gc->frame->pitch
  };

  return frame;
}

/** Used for calculating the distance a point is from a line segment.
 * This is used to determine if a point lies within a triangle.
 * @param point The point to get the distance of.
 * @param a The first point making up the line segment.
 * @param b The second point making up the line segment.
 * @returns The distance the point is from the line segment.
 * @ingroup baretype_impl
 * */
static btInt distFromLine(const btPoint* point, const btPoint* a, const btPoint* b) {
  return ((b->x - a->x) * (point->y - a->y))
       - ((b->y - a->y) * (point->x - a->x));
}

/** Calculates the distance between two points.
 * @param a The first point.
 * @param b The second point.
 * @returns The distance between the two points,
 * as a positive integer.
 * @ingroup baretype_impl
 * */
static btInt distFromPoint(const btPoint* a, const btPoint* b) {

  btPoint diff = {
    a->x - b->x,
    a->y - b->y
  };

  return lengthOfVector(&diff);
}

/** Locates the offset for a glyph.
 * @param typeface The typeface to get the glyph from.
 * @param charcode The character code to get the glyph of.
 * @param glyph_offset A pointer to the variable that receives
 * the glyph offset if it is found.
 * @returns Zero on success, an error code on failure.
 * @ingroup baretype_impl
 * */
static int findGlyph(const btTypeface* typeface,
                     btCharcode charcode,
                     btSize* glyph_offset_ptr) {

  if (typeface->buf_size < 24) {
    return BARETYPE_CORRUPT_TYPEFACE;
  }

  btCharcode charcode_begin = readU32(typeface->buf + 4);
  btCharcode charcode_count = readU32(typeface->buf + 8);

  if ((charcode < charcode_begin)
   || (charcode >= (charcode_begin + charcode_count))) {
    return BARETYPE_NOT_FOUND;
  }

  btSize glyph_table_offset = readU32(typeface->buf + 12);
  btSize glyph_entry_count  = readU32(typeface->buf + 16);
  btSize glyph_entry_size   = readU32(typeface->buf + 20);

  btSize glyph_index = charcode - charcode_begin;

  if (glyph_index >= glyph_entry_count) {
    return BARETYPE_CORRUPT_TYPEFACE;
  }

  btSize glyph_offset = glyph_table_offset + (glyph_index * glyph_entry_size);

  if ((glyph_offset + glyph_entry_size) >= typeface->buf_size) {
    return BARETYPE_CORRUPT_TYPEFACE;
  }

  *glyph_offset_ptr = glyph_offset;

  return 0;
}

/** Calculates the length of a positional 2D vector.
 * This can also be thought of as the length of the line
 * from the origin (0, 0) to the specified point.
 * This value is used to count the perimeter of a polygon.
 * @param point The point containing the vector components.
 * @returns The length of the vector.
 * @ingroup baretype_impl
 * */
static btInt lengthOfVector(const btPoint* point) {
  return calcSqrt((point->x * point->x) + (point->y * point->y));
}

/** Plots a physical dot.
 * A physical dot, as opposed to a virtual dot,
 * is a dot whose coordinates are absolute and
 * based on the frame's coordinate system. This
 * is opposed to a location in the glyph coordinate system.
 * @param frame The frame to plot the dot in.
 * @param point The point to plot the dot at.
 * @param value The value to add to the dot.
 * This may be between zero and @ref BARETYPE_DOT_MAX.
 * @ingroup baretype_impl
 * */
static void plotPixel(btFrame* frame, const btPoint* point, btInt value) {

  if ((point->x >= ((btInt) frame->width))
   || (point->y >= ((btInt) frame->height))) {
    return;
  } else if ((point->x < 0) || (point->y < 0)) {
    return;
  }

  btInt y = (frame->height - 1) - point->y;

  unsigned char* dst = frame->buf + ((y * frame->pitch) + point->x);

  if ((DOT_MAX - *dst) < value) {
    *dst = DOT_MAX;
  } else {
    *dst += value;
  }
}

/** Plots a virtual dot.
 * A virtual dot is considered to be a
 * subpixel location that is translated to
 * an actual pixel location.
 * @param frame The fram to plot the dot at.
 * @param virtualPoint The point to plot the dot at,
 * in units of sub pixels.
 * @param subpixels The number of subpixels in one pixel.
 * @ingroup baretype_impl
 * */
static void plotSubpixel(btFrame* frame, const btPoint* subpoint, btInt subpixels) {

  btPoint pixelpoint = {
    subpoint->x / subpixels,
    subpoint->y / subpixels
  };

  /* We're going to divide the subpixel
   * into four quadrants:
   *
   * Q0 | Q1
   * ---|---
   * Q2 | Q3
   *
   * The value gets distributed based
   * on how close the subpixel is to each
   * of this quadrants.
   * */

  /* The modulus point tells
   * us how close the subpixel
   * point is to each of these
   * quadrants. */

  btPoint modpoint = {
    subpoint->x % subpixels,
    subpoint->y % subpixels
  };

  btInt base_value = DOT_MAX;

  /* X left and right */
  btInt x_l = ((subpixels - modpoint.x) * base_value) / subpixels;
  btInt x_r = (modpoint.x * base_value) / subpixels;

  /* Y top and bottom */
  btInt y_t = ((subpixels - modpoint.y) * base_value) / subpixels;
  btInt y_b = (modpoint.y * base_value) / subpixels;

  btInt values[4] = {
    (x_l + y_t) / 2,
    (x_r + y_t) / 2,
    (x_l + y_b) / 2,
    (x_r + y_b) / 2
  };

  btPoint quadrants[4] = {
    { pixelpoint.x + 0, pixelpoint.y + 0 },
    { pixelpoint.x + 1, pixelpoint.y + 0 },
    { pixelpoint.x + 0, pixelpoint.y + 1 },
    { pixelpoint.x + 1, pixelpoint.y + 1 }
  };

  for (btSize i = 0; i < 4; i++) {
    plotPixel(frame, quadrants + i, values[i]);
  }
}

/** Reads a glyph structure from a buffer.
 * @param buf The buffer to read the glyph structure from.
 * The buffer must be at least 42 bytes long.
 * @returns A glyph structure decoded from the buffer.
 * @ingroup baretype_impl
 * */
static btGlyph readGlyph(const unsigned char* buf) {

  btGlyph glyph = {
    /* triangles */
    readTableSlice(buf),
    /* quadrilaterals */
    readTableSlice(buf),
    /* lines */
    readTableSlice(buf),
    /* quadratic Bezier curves */
    readTableSlice(buf),
    /* cubic Bezier curves */
    readTableSlice(buf),
    /* advance */
    readU16(buf)
  };

  return glyph;
}

/** Reads a table slice from a buffer.
 * @param buf The buffer to read the table slice from.
 * The buffer must be at least 8 bytes large.
 * @returns A decoded table slice.
 * @ingroup baretype_impl
 * */
static btTableSlice readTableSlice(const unsigned char* buf) {

  btTableSlice slice = {
    readU32(buf),
    readU32(buf + 4)
  };

  return slice;
}

/** Reads a 16-bit unsigned integer from a buffer.
 * @param buf The buffer to read the integer from.
 * @returns The decoded integer.
 * @ingroup baretype_impl
 * */
static unsigned short int readU16(const unsigned char* buf) {
  unsigned short int out = buf[0];
  return out | (buf[1] << 16);
}

/** Reads a 32-bit unsigned integer from a buffer.
 * @param buf The buffer to read the integer from.
 * @returns The decoded integer.
 * @ingroup baretype_impl
 * */
static unsigned long int readU32(const unsigned char* buf) {
  unsigned long int out = buf[0];
  out |= (buf[1] << 8);
  out |= (buf[2] << 16);
  return out | (buf[3] << 24);
}

/** Scales a vector to a certain magnitude.
 * @param in The vector to scale.
 * @param length The length to scale the vector to.
 * @returns The scaled vector.
 * @ingroup baretype_impl
 * */
static btPoint scaleVectorTo(const btPoint* in, btInt length) {

  btInt original_length = lengthOfVector(in);

  btPoint out = {
    (in->x * length) / original_length,
    (in->y * length) / original_length
  };

  return out;
}

/** Converts a font unit to a pixel coordinate.
 * @param gc An initialized graphics context.
 * @param in The point in font units to convert.
 * @returns A point in the pixel coordinate space.
 * @ingroup baretype_impl
 * */
static btPoint unitToPixel(const btGraphicsContext* gc, const btPoint* in) {

  btPoint out = {
    (in->x * gc->pixels_per_em) / gc->units_per_em,
    (in->y * gc->pixels_per_em) / gc->units_per_em
  };

  return out;
}

/** Converts a point from a unit coordinate
 * to a subpixel coordinate.
 * @param gc An initialized graphics context instance.
 * @param in The unit point to convert to a pixel point.
 * @returns The subpixel point version of @p in.
 * @ingroup baretype_impl
 * */
static btPoint unitToSubpixel(const btGraphicsContext* gc, const btPoint* in) {

  btPoint pixelpoint = unitToPixel(gc, in);

  btPoint out = {
    pixelpoint.x * gc->subpixels,
    pixelpoint.y * gc->subpixels
  };

  return out;
}

/** Indicates whether or not the given typeface is valid.
 * @param typeface The typeface to verify.
 * @returns Zero if the typeface is valid,
 * an error code if it's not.
 * @ingroup baretype_impl
 * */
static int verifyTypeface(const btTypeface* typeface) {

  if (typeface->buf_size < 4) {
    return BARETYPE_INVALID_TYPEFACE;
  }

  if ((typeface->buf[0] == 0xcd)
   && (typeface->buf[1] == 0xa4)
   && (typeface->buf[2] == 0x18)
   && (typeface->buf[3] == 0x4f)) {
    return 0;
  } else {
    return BARETYPE_INVALID_TYPEFACE;
  }
}

/* API functions start here. */

/** Initializes a graphics context.
 * @param frame The frame to draw on.
 * @returns An initialized graphics context.
 * @ingroup baretype_impl
 * */
static btGraphicsContext initGC(const btFrame* frame) {

  btPoint start = { 0, 0 };

  btGraphicsContext gc = {
    frame,
    &baretype_typeface,
    start,
    DEFAULT_PIXELS_PER_EM,
    DEFAULT_UNITS_PER_EM,
    DEFAULT_STROKE_WIDTH,
    DEFAULT_SUBPIXELS
  };

  return gc;
}

/** Draws a character.
 * @param gc An initialized graphics context.
 * @param ch The character to draw. This should
 * be considered a Unicode character code.
 * @returns Zero on success, @ref BT_ERROR_NOTFOUND
 * if the character could not be found in the typeface.
 * @ingroup baretype_impl
 * */
static btError drawChar(btGraphicsContext* gc, btCharcode ch) {

  btError err = verifyTypeface(gc->typeface);
  if (err) {
    return err;
  }

  btSize glyph_offset = 0;

  err = findGlyph(gc->typeface, ch, &glyph_offset);
  if (err) {
    return err;
  }

  btGlyph glyph = readGlyph(gc->typeface->buf + glyph_offset);

  for (btSize i = glyph.quadrilaterals.start; i < glyph.quadrilaterals.count; i++) {
  }

  return 0;
}

/** Draws a cubic bezier curve.
 * The width of the curve is based on the stroke width.
 * @param gc An initialized graphics context.
 * @param points The control points of the curve.
 * These must be in terms of font units.
 * @ingroup baretype_impl
 * */
static void drawCubicBezier(btGraphicsContext* gc, const btPoint* points) {

  /* First we need to find the points to the offset curves. */

  btPoint l_offsets[4];
  btPoint r_offsets[4];

  cubicOffsets(points, l_offsets, r_offsets, gc->stroke_width);

  /* Now we need to determine the suitable
   * number of lines to break the curve into. */

  btPoint subpixelPoints[4] = {
    unitToSubpixel(gc, points + 0),
    unitToSubpixel(gc, points + 1),
    unitToSubpixel(gc, points + 2),
    unitToSubpixel(gc, points + 3)
  };

  btInt t_max = calcPerimeter(subpixelPoints, 4);

  /* Now we render the curve. */

  btPoint last_points[2] = {
    l_offsets[0],
    r_offsets[0]
  };

  btPoint triangles[4];

  for (btInt t = 1; t < t_max; t++) {

    triangles[0] = last_points[0];
    triangles[1] = last_points[1];
    triangles[2] = cubicBezier(l_offsets, t, t_max);
    triangles[3] = cubicBezier(r_offsets, t, t_max);

    baretype.fillTriangle(gc, triangles + 0);
    baretype.fillTriangle(gc, triangles + 1);

    last_points[0] = triangles[2];
    last_points[1] = triangles[3];
  }
}

/** Draws a single line.
 * @param gc An initialized graphics context.
 * @param points The points making up the line.
 * There should be at least two points in this array.
 * @ingroup baretype_impl
 * */
static void drawLine(btGraphicsContext* gc, const btPoint* points) {

  btPoint l_offsets[2];
  btPoint r_offsets[2];

  lineOffsets(points, l_offsets, r_offsets, gc->stroke_width);

  btPoint sorted_offset_points[4] = {
    l_offsets[0],
    l_offsets[1],
    r_offsets[1],
    r_offsets[0]
  };

  baretype.fillQuadrilateral(gc, sorted_offset_points);
}

/** Draws a quadratic Bezier curve.
 * @param gc An initialized graphics context object.
 * @param points The points that define the curve.
 * There must be at least three points in this point array.
 * The points must also be in terms of font units.
 * @ingroup baretype_impl
 * */
static void drawQuadraticBezier(btGraphicsContext* gc, const btPoint* points) {

  /* First we need to get the control
   * points for the offset curves. */

  btPoint l_offsets[3];
  btPoint r_offsets[3];

  quadraticOffsets(points, l_offsets, r_offsets, gc->stroke_width);

  /* Now we need to determine a suitable
   * value for 't_max'. In otherwords, we
   * need to find out how many lines we're
   * going to break the curve into so that
   * it looks presentable without too much
   * of a performance cost. */

  btPoint pixelPoints[3] = {
    unitToPixel(gc, points + 0),
    unitToPixel(gc, points + 1),
    unitToPixel(gc, points + 2)
  };

  btInt t_max = calcPerimeter(pixelPoints, 3);

  /* Now we reconstruct the line at
   * the specified stroke width using
   * a large series of quadrilaterals. */

  btPoint last_points[2] = {
    l_offsets[0],
    r_offsets[0]
  };

  btPoint quadrilateral[4];

  for (btInt t = 1; t < t_max; t++) {

    quadrilateral[0] = last_points[0];
    quadrilateral[1] = quadraticBezier(l_offsets, t, t_max);
    quadrilateral[2] = quadraticBezier(r_offsets, t, t_max);
    quadrilateral[3] = last_points[1];

    baretype.fillQuadrilateral(gc, quadrilateral);

    last_points[0] = quadrilateral[1];
    last_points[1] = quadrilateral[2];
  }
}

/** Fills a triangle.
 * @param gc An initialized graphics context instance.
 * @param points The points making up the triangle.
 * @ingroup baretype_impl
 * */
static void fillTriangle(btGraphicsContext* gc, const btPoint* points) {

  btFrame cellFrame = createCellFrame(gc);

  btPoint a = unitToSubpixel(gc, points + 0);
  btPoint b = unitToSubpixel(gc, points + 1);
  btPoint c = unitToSubpixel(gc, points + 2);

  /* TODO : clip against frame width and height */

  btPoint max = {
    MAX3(a.x, b.x, c.x),
    MAX3(a.y, b.y, c.y)
  };

  btPoint min = {
    MIN3(a.x, b.x, c.x),
    MIN3(a.y, b.y, c.y)
  };

  btPoint point;

  btInt submask = gc->subpixels - 1;

  btPoint start = {
    (min.x + submask) & ~submask,
    (min.y + submask) & ~submask
  };

  for (point.y = start.y; point.y <= max.y; point.y += gc->subpixels) {
    for (point.x = start.x; point.x <= max.x; point.x += gc->subpixels) {
      btInt w0 = distFromLine(&point, &b, &c);
      btInt w1 = distFromLine(&point, &c, &a);
      btInt w2 = distFromLine(&point, &a, &b);
      if ((w0 >= 0) && (w1 >= 0) && (w2 >= 0)) {
        plotSubpixel(&cellFrame, &point, gc->subpixels);
      }
    }
  }
}

/** Fills a quadrilateral.
 * @param gc An initialized graphics context.
 * @param points The four points making up the quadrilateral.
 * These must be in terms of font units.
 * @ingroup baretype_impl
 * */
static void fillQuadrilateral(btGraphicsContext* gc, const btPoint* points) {

  btPoint t1[3] = {
    points[0],
    points[1],
    points[2]
  };

  btPoint t2[3] = {
    points[2],
    points[3],
    points[0]
  };

  baretype.fillTriangle(gc, t1);
  baretype.fillTriangle(gc, t2);
}

/** Accesses the units per EM.
 * @param gc An initialized graphics context instance.
 * @returns The number of units per EM.
 * @ingroup baretype_impl
 * */
static btInt getUnitsPerEM(const btGraphicsContext* gc) {
  return gc->units_per_em;
}

/** Sets the number of pixels per EM.
 * @param gc An initialized graphics context structure.
 * @param ppem The pixels per EM to assign.
 * @ingroup baretype_impl
 * */
static void setPixelsPerEM(btGraphicsContext* gc, btInt ppem) {
  if (ppem <= 0) {
    gc->pixels_per_em = DEFAULT_PIXELS_PER_EM;
  } else {
    gc->pixels_per_em = ppem;
  }
}

/** Sets the size of a stroke.
 * @param gc An initialized graphics context instance.
 * @param width The stroke width, in terms of font units, to assign.
 * @ingroup baretype_impl
 * */
static void setStrokeWidth(btGraphicsContext* gc, btInt width) {
  if (width <= 0) {
    gc->stroke_width = DEFAULT_STROKE_WIDTH;
  } else {
    gc->stroke_width = width;
  }
}

static void setTypeface(btGraphicsContext* gc, const btTypeface* typeface) {
  gc->typeface = typeface ? typeface : &baretype_typeface;
}

static const char* strerror(btError err) {

  switch (err) {
    case BARETYPE_NOT_FOUND:
      return "Glyph not found";
    case BARETYPE_CORRUPT_TYPEFACE:
      return "Typeface is corrupted";
    case BARETYPE_INVALID_TYPEFACE:
      return "Not a valid typeface";
    default:
      break;
  }

  return "";
}

const struct btLibrary baretype = {
  initGC,
  drawChar,
  drawCubicBezier,
  drawLine,
  drawQuadraticBezier,
  fillQuadrilateral,
  fillTriangle,
  getUnitsPerEM,
  setPixelsPerEM,
  setStrokeWidth,
  setTypeface,
  strerror
};
