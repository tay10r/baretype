BareType
========

Software that requires a font to be rendered is often faced with a decision:
either use a font rendering library or raster fonts, which don't look every good.

Embedded solutions will often use raster fonts, because existing font rendering libraries
will either have too many dependencies or will be too large.

This is where BareType comes in - a font rendering library with **zero dependencies** (no libc required), a **scalable typeface**, and an **easy to use** API.

All in just a set of three files:

 - an [API header](baretype.h) and [source file](baretype.c).
 - the [typeface definition file](baretype_typeface.c

You can learn [how to build](docs/Building.md) the library and [how to use it, too.](docs/Usage.md)
