PREFIX ?= ~/.local

CC := $(CROSS_COMPILE)gcc
AR := $(CROSS_COMPILE)ar

baretype_CFLAGS := -Wall -Wextra -Werror -Wfatal-errors

.PHONY: all
all: libbaretype.a

libbaretype.a: baretype.o baretype_typeface.o
	$(AR) $(ARFLAGS) $@ $^

baretype.o: baretype.c baretype.h
	$(CC) $(baretype_CFLAGS) $(CFLAGS) -c $< -o $@

baretype_typeface.o: baretype_typeface.c baretype.h

.PHONY: clean
clean:
	$(RM) *.o libbaretype.a

.PHONY: examples
examples:
	$(MAKE) -C examples TOP=$(CURDIR)

.PHONY: examples_clean
examples_clean:
	$(MAKE) -C examples clean TOP=$(CURDIR)

.PHONY: install
install:
	cp libbaretype.a $(PREFIX)/lib
	cp baretype.h $(PREFIX)/include
