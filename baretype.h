#ifndef BARETYPE_BARETYPE_H
#define BARETYPE_BARETYPE_H

/** @file baretype.h
 * @brief The header for the BareType library.
 * See @ref baretype_api for usage details.
 * */

/** @defgroup baretype_api Public API
 * @brief Declarations related to the
 * usage of the library. See @ref btLibrary::init
 * to get started.
 * */

/** An error code returned when
 * a glyph for a certain character
 * could not be found.
 * @ingroup baretype_api
 * */
#define BARETYPE_NOT_FOUND 1

/** An error code returned when a
 * typeface file is not the supported format.
 * @ingroup baretype_api
 * */
#define BARETYPE_INVALID_TYPEFACE 2

/** An error code returned when
 * a typeface file is missing data.
 * @ingroup baretype_api
 * */
#define BARETYPE_CORRUPT_TYPEFACE 4

#ifdef __cplusplus
extern "C" {
#endif

/** A type definition for a character code.
 * This type is an unsigned long integer because
 * it needs to be at least 32-bits long to fit a unicode character.
 * @ingroup baretype_api
 * */
typedef unsigned long int btCharcode;

/** A type definition for an error value.
 * @ingroup baretype_api
 * */
typedef int btError;

/** A type definition for a integer type.
 * Used for most math calculations in the library.
 * This could also be a "long long int", to ensure
 * that the type is 64-bits long. A normal "int" should
 * work for most use cases though.
 * @ingroup baretype_api
 * */
typedef long int btInt;

/** A type definition for a size value.
 * Size values are used for array lengths,
 * width and height specifications, and indices.
 * @ingroup baretype_api
 * */
typedef unsigned long int btSize;

/** Contains information regarding a render frame.
 * This structure should be declared by the user
 * before calling @ref btLibrary::init.
 * @ingroup baretype_api
 * */
struct btFrame {
  /** The frame buffer. */
  unsigned char* buf;
  /** The number of pixels in the frame. */
  btSize width;
  /** The number of lines in the frame. */
  btSize height;
  /** The number of pixels between lines. */
  btSize pitch;
};

/** A type definition for the frame structure.
 * @ingroup baretype_api
 * */
typedef struct btFrame btFrame;

/** Contains a 2D coordinate.
 * The coordinate may use to refer
 * to a location on the frame buffer
 * or to a location in glyph space.
 * @ingroup baretype_api
 * */
struct btPoint {
  /** The X coordinate of the point. */
  btInt x;
  /** The Y coordinate of the point. */
  btInt y;
};

/** A type definition for the point structure.
 * @ingroup baretype_api
 * */
typedef struct btPoint btPoint;

/** Used to contain a buffer that
 * describes the typeface to be drawn by the library.
 * @ingroup baretype_api
 * */
struct btTypeface {
  /** The buffer containing the typeface. */
  const unsigned char* buf;
  /** The size of the buffer containing the typeface. */
  btSize buf_size;
};

/** A type definition for the
 * typeface structure.
 * @ingroup baretype_api
 * */
typedef struct btTypeface btTypeface;

/** A graphics context structure.
 * This structure is used to indicate
 * where the library should render the
 * characters at and what properties used
 * to render the typeface.
 * @ingroup baretype_api
 * */
struct btGraphicsContext {
  /** The frame on which the glyphs
   * will be rendered to. */
  const btFrame* frame;
  /** The typeface to be drawn. */
  const btTypeface* typeface;
  /** The position at which the next
   * character will be drawn. */
  btPoint pen_position;
  /** The number of pixels per EM.  */
  btInt pixels_per_em;
  /** The number of font units per EM. */
  btInt units_per_em;
  /** The width of a line stroke, in
   * terms of font units. */
  btInt stroke_width;
  /** The number of subpixels used
   * in the rendering process. */
  btInt subpixels;
};

/** A type definition for the graphics context structure.
 * @ingroup baretype_api
 * */
typedef struct btGraphicsContext btGraphicsContext;

/** This structure is used
 * to contain the interface to
 * the library, will reducing
 * the number of symbols exported.
 * @ingroup baretype_api
 * */
struct btLibrary {
  /** Initializes a graphics context.
   * A frame must have been already declared. */
  btGraphicsContext (*init)(const btFrame* frame);
  /** Draws a character onto the frame.
   * This function returns @ref BT_ERROR_NOTFOUND
   * if the character could not be found in the typeface.
   * */
  btError (*drawChar)(btGraphicsContext* gc, btCharcode ch);
  /** Draws a cubic Bezier curve.
   * There must be at least four points in
   * the point array and they must be in font units.
   * The "steps" parameter is used to indicate how many
   * lines the curve should be broken up to. The quality
   * of the curve and the performance costs of this function
   * are both directly proportional to the step count.
   * */
  void (*drawCubicBezier)(btGraphicsContext* gc, const btPoint* points);
  /** Draws a single line.
   * There must be at least two points in the point
   * array and they must be in font units. */
  void (*drawLine)(btGraphicsContext* gc, const btPoint* points);
  /** Draws a quadratic Bezier curve.
   * There must be at least three points in the point array
   * and they must be in terms of font units. Use @ref btLibrary::getUnitsPerEM
   * to determine the maximum value of a font unit. */
  void (*drawQuadraticBezier)(btGraphicsContext* gc, const btPoint* points);
  /** Fills a quadrilateral specified by four points.
   * All four points must be in font units.  */
  void (*fillQuadrilateral)(btGraphicsContext* gc, const btPoint* points);
  /** Fills a triangle specified by a set of points.
   * There must be at least three points in the point array
   * and they should be specified in font units. */
  void (*fillTriangle)(btGraphicsContext* gc, const btPoint* points);
  /** Gets the number of units per EM.
   * This is useful if using the triangle rendering function. */
  btInt (*getUnitsPerEM)(const btGraphicsContext* gc);
  /** Sets the number of pixels per EM. */
  void (*setPixelsPerEM)(btGraphicsContext* gc, btInt ppem);
  /** Sets the stroke width.
   * This will effect how thick a font is rendered.
   * The width is described in terms of font units (or units per EM). */
  void (*setStrokeWidth)(btGraphicsContext* gc, btInt width);
  /** Assigns the typeface to be drawn. */
  void (*setTypeface)(btGraphicsContext* gc, const btTypeface* typeface);
  /** Converts an error code to a human-readable string. */
  const char* (*strerror)(btError err);
};

/** This is the declaration of the library interface.
 * Functions are called this way so that only one
 * symbol has to be exported from the library.
 * @ingroup baretype_api
 * */
extern const struct btLibrary baretype;

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* BARETYPE_BARETYPE_H */
