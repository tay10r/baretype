#include "Controller.hpp"

#include <baretype.h>

#include <QFileDialog>
#include <QString>

#include <fstream>
#include <memory>
#include <vector>

namespace bt {

namespace {

/// The default typeface filename.
const char defFilename[] = "Untitled Typeface.bin";

} // namespace

Controller::Controller() : filename(defFilename) {

}

bool Controller::openDefault() {
  return this->open(defFilename);
}

bool Controller::open(const char* filename_) {

  this->filename = filename_ ? filename : "";

  std::ifstream file(filename.c_str(), std::ios::in | std::ios::binary);
  if (!file.good()) {
    return false;
  }

  file.seekg(0, std::ios::end);

  this->body.reserve(file.tellg());

  file.seekg(0, std::ios::beg);

  this->body.assign((std::istreambuf_iterator<char>(file)),
                    std::istreambuf_iterator<char>());

  return true;
}

bool Controller::save() {

  std::ofstream file(filename.c_str(), std::ios::out | std::ios::binary);
  if (!file.good()) {
    return false;
  }

  file << this->body;

  return true;
}

void Controller::handleOpen() {

  auto filename_ = QFileDialog::getOpenFileName();

  this->open(filename_.toStdString().c_str());

  emit typefaceUpdated((const unsigned char*) this->body.data(), this->body.size());
}

void Controller::handleSave(const QString& text) {
  this->body = text.toStdString();
  this->save();
}

void Controller::handleQuit() {
  emit exiting();
}

} // namespace bt
