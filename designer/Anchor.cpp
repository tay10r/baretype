#include "Anchor.hpp"

#include <QPainter>
#include <QSize>

namespace bt {

Anchor::Anchor(const QPointF& pos, QObject* parent) : QObject(parent), position(pos) { }

void Anchor::rescale(const QSize& oldSize, const QSize& nextSize) {
  this->position.setX((this->position.x() * nextSize.width())  / oldSize.width());
  this->position.setY((this->position.y() * nextSize.height()) / oldSize.height());
}

void CircleAnchor::paint(QPainter& painter) const {
  painter.drawEllipse(this->getPosition(), this->radius, this->radius);
}

} // namespace bt
