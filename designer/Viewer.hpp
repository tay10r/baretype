#ifndef BARETYPE_DESIGNER_VIEWER_HPP
#define BARETYPE_DESIGNER_VIEWER_HPP

#include <QScrollArea>

class QResizeEvent;

namespace bt {

class Canvas;

/// Used for previewing the rendering
/// of the typeface being designed.
/// @ingroup bt
class Viewer : public QScrollArea {
  Q_OBJECT
public:
  /// Constructs the viewer.
  /// @param parent A pointer to the parent widget.
  Viewer(QWidget* parent);
  /// Updates the preview of the typeface.
  void updatePreview();
  /// Zooms out of the drawn image.
  void zoomIn();
  /// Zooms in to the drawn image.
  void zoomOut();
  /// Enables the grid.
  void enableGrid();
  /// Disables the grid.
  void disableGrid();
protected:
  /// Handles a window resize event.
  /// @param resizeEvent A pointer to the resize event.
  void resizeEvent(QResizeEvent* resizeEvent) override;
private:
  /// The widget in which the
  /// characters are drawn.
  Canvas* canvas;
};

} // namespace bt

#endif // BARETYPE_DESIGNER_VIEWER_HPP
