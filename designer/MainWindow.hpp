#ifndef BARETYPE_DESIGNER_MAIN_WINDOW_HPP
#define BARETYPE_DESIGNER_MAIN_WINDOW_HPP

#include <QMainWindow>

class QAction;
class QMenu;

namespace bt {

class Viewer;

/// The root window of the application.
/// @ingroup bt
class MainWindow : public QMainWindow {
  Q_OBJECT
public:
  /// Constructs the main window.
  MainWindow();
signals:
  /// Emitted when the "Quit" button
  /// is clicked.
  void quitRequested();
  /// Emitted when the "Save" button is clicked.
  /// @param text The body of text present
  /// when the "Save" button was hit.
  void saveRequested(const QString& text);
  /// Emitted when the "Open" button is clicked.
  void openRequested();
public slots:
  /// Handles an exiting state from the
  /// application controller.
  void handleExit();
  /// Updates the typeface being drawn.
  /// @param buf The buffer containing the typeface info.
  /// @param bufSize The size of the typeface buffer.
  void updateTypeface(const unsigned char* buf, std::size_t bufSize);
protected slots:
  /// Handles the "Zoom In" button
  /// being clicked.
  void handleZoomIn();
  /// Handles the "Zoom Out" button
  /// being clicked.
  void handleZoomOut();
  /// Handles the "Quit" button
  /// being clicked.
  void handleQuit();
  /// Handles the "Open" button
  /// being clicked.
  void handleOpen();
  /// Handles the "Save" button
  /// being clicked.
  void handleSave();
private:
  /// The file menu.
  QMenu* fileMenu;
  /// The edit menu.
  QMenu* editMenu;
  /// The view menu.
  QMenu* viewMenu;
  /// The help menu.
  QMenu* helpMenu;
  /// Opens a file for editing.
  QAction* openAction;
  /// Exits the designer program.
  QAction* quitAction;
  /// Zooms into the typeface.
  QAction* zoomInAction;
  /// Zooms out of the typeface.
  QAction* zoomOutAction;
  /// The action that
  /// saves the contents
  /// of the DSL file.
  QAction* saveAction;
  /// The action used to
  /// undo the modification
  /// of the DSL file.
  QAction* undoAction;
  /// The action used to
  /// redo the modification
  /// of the DSL file.
  QAction* redoAction;
  /// The type face viewer widget.
  Viewer* viewer;
};

} /* namespace bt */

#endif // BARETYPE_DESIGNER_MAIN_WINDOW_HPP
