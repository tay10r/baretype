#include "MainWindow.hpp"

#include "Viewer.hpp"

#include <QMenu>
#include <QMenuBar>

#include <sstream>

namespace bt {

MainWindow::MainWindow() {

  this->fileMenu = new QMenu(tr("File"), this);

  this->openAction = this->fileMenu->addAction(tr("Open"));
  this->openAction->setShortcut(Qt::Key_O | Qt::CTRL);

  this->saveAction = this->fileMenu->addAction(tr("Save"));
  this->saveAction->setShortcut(Qt::Key_S | Qt::CTRL);

  this->quitAction = this->fileMenu->addAction(tr("Quit"));
  this->quitAction->setShortcut(Qt::Key_Q | Qt::CTRL);

  this->editMenu = new QMenu(tr("Edit"), this);
  this->undoAction  = this->editMenu->addAction(tr("Undo"));
  this->redoAction  = this->editMenu->addAction(tr("Redo"));

  this->viewMenu = new QMenu(tr("View"), this);

  this->zoomInAction = this->viewMenu->addAction(tr("Zoom In"));
  this->zoomInAction->setShortcut(Qt::Key_Equal | Qt::CTRL);

  this->zoomOutAction = this->viewMenu->addAction(tr("Zoom Out"));
  this->zoomOutAction->setShortcut(Qt::Key_Minus | Qt::CTRL);

  this->helpMenu = new QMenu(tr("Help"), this);

  this->menuBar()->addMenu(this->fileMenu);
  this->menuBar()->addMenu(this->editMenu);
  this->menuBar()->addMenu(this->viewMenu);
  this->menuBar()->addMenu(this->helpMenu);

  this->viewer = new Viewer(this);

  this->setCentralWidget(this->viewer);

  this->setWindowTitle(tr("BareType Designer"));

  QObject::connect(this->zoomInAction,  &QAction::triggered, this, &MainWindow::handleZoomIn);
  QObject::connect(this->zoomOutAction, &QAction::triggered, this, &MainWindow::handleZoomOut);

  QObject::connect(this->quitAction,  &QAction::triggered, this, &MainWindow::handleQuit);
  QObject::connect(this->openAction,  &QAction::triggered, this, &MainWindow::handleOpen);
  QObject::connect(this->saveAction,  &QAction::triggered, this, &MainWindow::handleSave);
}

void MainWindow::handleZoomIn() {
  this->viewer->zoomIn();
}

void MainWindow::handleZoomOut() {
  this->viewer->zoomOut();
}

void MainWindow::handleQuit() {
  emit quitRequested();
}

void MainWindow::handleOpen() {
  emit openRequested();
}

void MainWindow::handleSave() {

}

void MainWindow::handleExit() {
  this->close();
}

void MainWindow::updateTypeface(const unsigned char* buf, std::size_t bufSize) {
  (void)buf;
  (void)bufSize;
}

} // namespace bt
