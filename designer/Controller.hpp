#ifndef BARETYPE_DESIGNER_CONTROLLER_HPP
#define BARETYPE_DESIGNER_CONTROLLER_HPP

#include <QObject>

#include <string>

namespace bt {

/// Used for controlling
/// the application logic.
class Controller : public QObject {
  Q_OBJECT
public:
  /// Constructs the controller.
  Controller();
  /// Opens the default typeface file.
  /// @returns True on success, false on failure.
  bool openDefault();
  /// Opens a typeface file.
  /// @param filename_ The path to the file to open.
  /// @returns True on success, false on failure.
  bool open(const char* filename_);
  /// Saves the content of the opened file.
  /// @returns True on success, false on failure.
  bool save();
signals:
  /// Emitted when the application
  /// is exiting.
  void exiting();
  /// Emitted when a new typeface is opened.
  /// @param buf The buffer containing the typeface.
  /// @param buf_size The size of the buffer containing the typeface.
  void typefaceUpdated(const unsigned char* buf, std::size_t buf_size);
public slots:
  /// Handles a "Quit" request.
  void handleQuit();
  /// Handles a "Save" request.
  /// @param text The text that was
  /// present when the "Save" button was hit.
  void handleSave(const QString& text);
  /// Handles an "Open" request.
  void handleOpen();
protected:
  /// Builds the currently opened file.
  /// @returns True on success, false on failure.
  bool build();
private:
  /// The name of the file
  /// being worked on.
  std::string filename;
  /// The body of the typeface file.
  std::string body;
};

} // namespace bt

#endif // BARETYPE_DESIGNER_CONTROLLER_HPP
