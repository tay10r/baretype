#ifndef BARETYPE_DESIGNER_ANCHOR_HPP
#define BARETYPE_DESIGNER_ANCHOR_HPP

#include <QColor>
#include <QObject>
#include <QPointF>

class QPainter;
class QSize;

namespace bt {

/// Used to mark a location on the canvas.
class Anchor : public QObject {
  Q_OBJECT
public:
  /// Constructs the anchor.
  /// @param position_ The position that
  /// this anchor resides at.
  /// @param parent The parent object.
  Anchor(const QPointF& position_, QObject* parent = nullptr);
  /// Rescales the anchor on a new coordinate space.
  /// @param old The old coordinate space.
  /// @param nextSize The next coordinate space.
  virtual void rescale(const QSize& oldSize, const QSize& nextSize);
  /// Paints the anchor.
  /// @param painter The painter to drawn the shapes with.
  virtual void paint(QPainter& painter) const = 0;
protected:
  /// Accesses the position.
  /// @returns The anchor's position.
  QPointF getPosition() const {
    return this->position;
  }
private:
  /// The position that the anchor resides at.
  QPointF position;
};

/// A circle shape anchor.
class CircleAnchor : public Anchor {
  /// The color of the circle.
  QColor color;
  /// The radius of the circle.
  qreal radius;
public:
  /// Constructs the circle anchor.
  /// @param position The position of this anchor.
  /// @param parent A pointer to the parent object.
  CircleAnchor(const QPointF& position, QObject* parent)
    : Anchor(position, parent), color("red"), radius(10) { }
  /// Paints the circle.
  /// @param painter The painter to draw with.
  void paint(QPainter& painter) const override;
  /// Assigns the radius of the circle.
  /// @param radius The radius to assign the circle.
  void setRadius(qreal r) {
    this->radius = r;
  }
};

} // namespace bt

#endif // BARETYPE_DESIGNER_ANCHOR_HPP
