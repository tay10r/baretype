#include <QApplication>

/// @defgroup baretype_designer Designer App
/// @brief Declarations used in the implementation
/// of the typeface designer.

#include "Controller.hpp"
#include "MainWindow.hpp"

int main(int argc, char** argv) {

  using namespace bt;

  QApplication app(argc, argv);

  Controller controller;

  MainWindow mainWindow;
  mainWindow.resize(1366, 768);
  mainWindow.show();

  QObject::connect(&mainWindow, &MainWindow::openRequested,
                   &controller, &Controller::handleOpen);

  QObject::connect(&mainWindow, &MainWindow::saveRequested,
                   &controller, &Controller::handleSave);

  QObject::connect(&mainWindow, &MainWindow::quitRequested,
                   &controller, &Controller::handleQuit);

  QObject::connect(&controller, &Controller::typefaceUpdated,
                   &mainWindow, &MainWindow::updateTypeface);

  QObject::connect(&controller, &Controller::exiting,
                   &mainWindow, &MainWindow::handleExit);

  controller.openDefault();

  return app.exec();
}
