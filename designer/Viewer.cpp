#include "Viewer.hpp"

#include "Anchor.hpp"

#include <baretype.h>

#include <QColor>
#include <QPainter>
#include <QImage>
#include <QList>
#include <QMouseEvent>
#include <QResizeEvent>

#include <cmath>

namespace bt {

/// This is the widget that the
/// content is drawn on.
class Canvas : public QWidget {
public:
  /// Constructs the frame.
  /// @param parent A pointer to
  /// the parent widget.
  Canvas(QWidget* parent) : QWidget(parent) {
    this->xOffset = 0;
    this->yOffset = 0;
    this->pixelsPerSquare = 32;
    this->gridFlag = true;
    this->snapFlag = true;
  }
  /// Redraws the frame.
  void redraw() {
    this->redraw(this->size());
  }
  /// Calculates the zoom in and out size delta.
  /// @param s The size to get the delta of.
  /// @returns The size delta, which can be
  /// either added to or subtracted from the
  /// current size. This achieves the effect
  /// of zooming in or out, respectively.
  QSize calcDeltaSize(const QSize& s) {

    const int percent = 12;

    QSize out((s.width()  * percent) / 100,
              (s.height() * percent) / 100);

    return out;
  }
  /// Zooms into the image.
  void zoomIn() {

    auto oldSize = this->size();

    auto deltaSize = this->calcDeltaSize(oldSize);

    auto newSize = QSize(oldSize.width()  + deltaSize.width(),
                         oldSize.height() + deltaSize.height());

    this->resize(newSize);
  }
  /// Zooms out of the image.
  void zoomOut() {

    auto oldSize = this->size();

    auto deltaSize = this->calcDeltaSize(oldSize);

    auto newSize = QSize(oldSize.width()  - deltaSize.width(),
                         oldSize.height() - deltaSize.height());

    this->resize(newSize);
  }
  /// Enables the grid.
  void enableGrid() {
    if (!this->gridFlag) {
      this->gridFlag = true;
      this->repaint();
    }
  }
  /// Disables the grid;
  void disableGrid() {
    if (this->gridFlag) {
      this->gridFlag = false;
      this->repaint();
    }
  }
protected:
  /// Snaps a point to the grid.
  /// @param in The point to snap.
  /// @returns The point at the nearest grid location.
  QPointF snapPosition(const QPointF& in) {

    qreal pps = this->pixelsPerSquare;

    //qreal hGrid = this->width()  / pps;
    //qreal vGrid = this->height() / pps;

    qreal hPoint = in.x() / pps;
    qreal vPoint = in.y() / pps;

    QPointF out(std::round(hPoint) * pps,
                std::round(vPoint) * pps);

    return out;
  }
  /// Handles a mouse click event.
  /// @param mouseEvent The event information instance.
  void mousePressEvent(QMouseEvent* mouseEvent) override {

    auto pos = mouseEvent->localPos();

    auto anchorPos = snapFlag ? this->snapPosition(pos) : pos;

    this->anchors.push_back(new CircleAnchor(anchorPos, this));

    this->repaint();
  }
  /// Redraws the text.
  /// @param imageSize The size of the
  /// image to draw on.
  void redraw(const QSize& imageSize) {

    // Resize the Qt image

    this->glyphImage = QImage(imageSize, QImage::Format_Grayscale8);

    // Create the BareType frame

    btSize width  = imageSize.width();
    btSize height = imageSize.height();
    btSize pitch  = width;

    std::vector<unsigned char> frameBuf(pitch * height);

    btFrame frame = {
      frameBuf.data(),
      width,
      height,
      pitch
    };

    // Redraw the frame

    btInt pixelsPerEM = std::min(width, height);

    auto gc = baretype.init(&frame);

    baretype.setPixelsPerEM(&gc, pixelsPerEM);

    baretype.drawChar(&gc, 'a');

    btPoint points[3] = {
      { 0, 0 },
      { 512, 512 },
      { 128, 256 }
    };

    baretype.fillTriangle(&gc, points);

    // Move the bits onto the Qt image instance.

    for (btSize y = 0; y < height; y++) {

      for (btSize x = 0; x < width; x++) {

        int value = 255 - frameBuf[(y * pitch) + x];

        this->glyphImage.setPixelColor(x, y, QColor(value, value, value));
      }
    }
  }
  /// Overrides the paint event
  /// so that the font face can be
  /// written to the widget.
  void paintEvent(QPaintEvent*) override {

    QPainter painter(this);

    this->paintGlyphImage(painter);

    if (this->gridFlag) {
      this->paintNotches(painter);
    }

    this->paintAnchors(painter);
  }
  /// Paints the glyph image onto the canvas.
  /// @param painter The initialized painter instance.
  void paintGlyphImage(QPainter& painter) {

    QPoint targetPos(0, 0);

    painter.drawImage(targetPos, this->glyphImage);
  }
  /// Paints the notches on the widget.
  /// @param painter An initialized painter object.
  /// This object calls the draw functions.
  void paintNotches(QPainter& painter) {

    painter.setPen(this->makeGridPen());

    auto windowSize = this->size();

    auto xStart = this->xOffset % windowSize.width();
    auto yStart = this->yOffset % windowSize.height();

    // Draw horizontal lines
    for (auto y = yStart; y < windowSize.height(); y += this->pixelsPerSquare) {
      painter.drawLine(0, y, windowSize.width(), y);
    }

    // Draw vertical lines
    for (auto x = xStart; x < windowSize.width(); x += this->pixelsPerSquare) {
      painter.drawLine(x, 0, x, windowSize.height());
    }
  }
  /// Paints the active anchors.
  /// @param painter The painter to draw shapes with.
  void paintAnchors(QPainter& painter) {
    for (auto* anchor : this->anchors) {
      anchor->paint(painter);
    }
  }
  /// Resizes the frame buffer and
  /// the image based on the new widget size.
  /// @param event A pointer to the resize event.
  void resizeEvent(QResizeEvent* event) override {

    auto nextSize = event->size();

    this->redraw(nextSize);

    QWidget::resizeEvent(event);
  }
  /// Creates a pen suitable for drawing the grid.
  /// @returns A pen for drawing the grid.
  QPen makeGridPen() {
    QPen pen(QColor(0, 0, 0, 64));
    return pen;
  }
private:
  /// An image of the glyph being displayed.
  QImage glyphImage;
  /// The anchors plotted on the canvas by the user.
  QList<Anchor*> anchors;
  /// The X offset of the grid.
  int xOffset;
  /// The Y offset of the grid.
  int yOffset;
  /// The number of pixels in one
  /// grid square.
  int pixelsPerSquare;
  /// Whether or not to enable
  /// the grid view.
  bool gridFlag;
  /// Whether or not to snap
  /// anchors to a grid intersection.
  bool snapFlag;
};

Viewer::Viewer(QWidget* parent) : QScrollArea(parent) {

  this->canvas = new Canvas(this);

  this->canvas->setMinimumSize(this->size());

  this->setWidget(this->canvas);
}

void Viewer::updatePreview() {
  this->canvas->redraw();
  this->canvas->repaint();
}

void Viewer::zoomIn() {
  canvas->zoomIn();
}

void Viewer::zoomOut() {
  canvas->zoomOut();
}

void Viewer::enableGrid() {
  canvas->enableGrid();
}

void Viewer::disableGrid() {
  canvas->disableGrid();
}

void Viewer::resizeEvent(QResizeEvent* resizeEvent) {

  this->canvas->setMinimumSize(resizeEvent->size());

  QScrollArea::resizeEvent(resizeEvent);
}

} // namespace bt
